#!/usr/bin/env python3
"""
This files handles e-mail notification when files uploaded to web server.
Reference: https://www.twilio.com/blog/sending-email-attachments-with-twilio-sendgrid-python
"""
import base64
from pathlib import Path
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import (Mail, Attachment, FileContent, FileName, FileType, Disposition)


class EmailNotifier:
    @staticmethod
    def send_email(from_address, to_address_list, subject, message, file_attachment):
        msg = Mail(
            from_email=from_address,
            to_emails=to_address_list,
            subject=subject,
            html_content=message
        )

        if file_attachment is not None:
            with open(file_attachment, "rb") as f:
                data = f.read()
            encoded_file = base64.b64encode(data).decode()

            attached_filename = Path(file_attachment).name
            attached_file = Attachment(
                FileContent(encoded_file),
                FileName(attached_filename),
                FileType('application/txt'),
                Disposition('attachment')
            )
            msg.attachment = attached_file

        sg = SendGridAPIClient("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")
        response = sg.send(msg)
        print(response.status_code, response.body, response.headers)


if __name__ == "__main__":
    email = EmailNotifier()
    email.send_email(from_address='cacc.server@gmail.com',
                     to_address_list=['peterkft@gmail.com', 'cacc.server@gmail.com'],
                     subject='SUBJECT OF THE MAIL',
                     message='BODY OF THE MAIL',
                     file_attachment='log/20221001.txt')
