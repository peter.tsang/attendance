import pytest
from datetime import datetime
from attendance_file import *


class TestAttendanceFile:
    @staticmethod
    def get_next_sunday_date(date):
        current_date = datetime(int(date[:4]), int(date[4:6]), int(date[6:8]))
        iso_day_of_week = current_date.isoweekday()
        next_sunday_date = current_date
        if iso_day_of_week != 7:
            next_sunday_date += timedelta(7 - iso_day_of_week)
        next_sunday_date = next_sunday_date.strftime('%Y%m%d')
        return next_sunday_date

    def test_sermon_file_datetime_sanity_check(self):
        """
        Test the outcome of filename with the key is not on Sunday date.
        """
        file_key = "20181231"
        attendance_file = AttendanceFile(file_key=file_key)
        file_key = attendance_file.get_attendance_file_key()
        next_sun_date = attendance_file.get_next_sunday_date(key=file_key)
        filename = attendance_file.get_next_weekend_date_filename()
        assert "20181231" == file_key
        assert "20190106" == next_sun_date
        assert "20190106.txt" == filename

    def test_sermon_file_datetime_with_no_key(self):
        """
        Test the outcome of filename with the key is None.
        """
        attendance_file = AttendanceFile(file_key=None)
        filename = attendance_file.get_next_weekend_date_filename()
        file_key = attendance_file.get_attendance_file_key()
        today = datetime.today()
        current_date = today.strftime('%Y%m%d')
        next_sunday_date = self.get_next_sunday_date(current_date)
        next_sunday_filename = '{}.txt'.format(next_sunday_date)
        assert file_key == current_date
        # assert next_sunday_date == attendance_file.get_next_sunday_date(current_date)
        assert next_sunday_filename == filename

    def test_sermon_file_datetime_with_key_not_integer(self):
        """
        Tests the outcome of the filename with the key is not a valid datetime key.
        """
        attendance_file = AttendanceFile(file_key='caccalliance')
        filename = attendance_file.get_next_weekend_date_filename()
        file_key = attendance_file.get_attendance_file_key()
        today = datetime.today()
        current_date = today.strftime('%Y%m%d')
        next_sunday_date = self.get_next_sunday_date(current_date)
        next_sunday_filename = '{}.txt'.format(next_sunday_date)
        assert file_key == current_date
        assert next_sunday_date == attendance_file.get_next_sunday_date(current_date)
        assert next_sunday_filename == filename

    def test_sermon_file_date_is_exactly_on_sunday(self):
        """
        Tests the outcome of the filename with the key is exactly on sunday.
        """
        file_key = "20190106"
        attendance_file = AttendanceFile(file_key=file_key)
        file_key = attendance_file.get_attendance_file_key()
        next_sun_date = attendance_file.get_next_sunday_date(key=file_key)
        filename = attendance_file.get_next_weekend_date_filename()
        assert "20190106" == file_key
        assert "20190106" == next_sun_date
        assert "20190106.txt" == filename
