#!/usr/bin/env python3
"""
This file is used to upload attendance file to FTP server
"""
import click
import logging
import netrc
import pysftp
import os
from pathlib import Path
from attendance_file import AttendanceFile
from sendgrid_notifier import EmailNotifier

_EOF_WINDOWS = '\r\n'
_EOF_LINUX = '\n'
LOGGER_FOLDER = Path("attendance").absolute()
DEFAULT_FOLDER = LOGGER_FOLDER / "record"


@click.command()
@click.option('--override_create_key', type=click.STRING, help='Override create key')
@click.option('--verbose', '-v', is_flag=True, help='Enable verbose mode')
class AttendanceFileUploader:
    def __init__(self, override_create_key: None, verbose: False):
        self.override_create_key = override_create_key
        self.verbose = verbose
        self.attendance_file = AttendanceFile(self.override_create_key)

        # Get create key
        filename = self.attendance_file.get_next_weekend_date_filename()
        filename_no_ext, filename_extension = os.path.splitext(filename)

        formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
        trace_logger = logging.getLogger()
        trace_logger.setLevel(logging.DEBUG)

        log_handler = logging.StreamHandler()
        log_handler.setLevel(logging.DEBUG)
        log_handler.setFormatter(formatter)
        trace_logger.addHandler(log_handler)

        full_path_in_filename = '{0}/{1}{2}'.format(DEFAULT_FOLDER, filename_no_ext, filename_extension)
        self.full_path_upload_folder = DEFAULT_FOLDER / "upload"

        self.create_folder_if_not_exist()
        full_path_out_filename = '{0}/{1}_mod{2}'.format(self.full_path_upload_folder,
                                                         filename_no_ext,
                                                         filename_extension)

        logging.debug('in_filename: {}'.format(full_path_in_filename))

        try:
            content = open(full_path_in_filename).read()
            # Clear empty line if any.
            content = content.strip()
            num_of_attendance = len(content.splitlines())
            logging.debug('Num of attendance: {}'.format(num_of_attendance))
        except IOError:
            logging.error('{} is not existed.'.format(full_path_out_filename))
            return

        # Unify line ending to Linux line ending in case the file consists a mixture of line endings.
        #  Convert everything back Windows line ending and save to a new file.
        content = content.replace(_EOF_WINDOWS, _EOF_LINUX)
        content = content.replace(_EOF_LINUX, _EOF_WINDOWS)

        with open(full_path_out_filename, 'w') as out_file:
            out_file.write(content)
        logging.debug('out_filename: {}'.format(full_path_out_filename))

        self.file_key = filename_no_ext

        self.ftp_host = '192.168.0.100'
        self.ftp_host_port_no = 20022
        auth = netrc.netrc().authenticators(self.ftp_host)
        username = auth[0]
        password = auth[2]
        logging.info("Start SFTP connection")
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        with pysftp.Connection(self.ftp_host, username=username, password=password, cnopts=cnopts,
                               port=self.ftp_host_port_no) as sftp:
            with sftp.cd('attendance'):
                sftp.put(full_path_out_filename)
            logging.info('Upload {} to attendance folder'.format(full_path_out_filename))

        # Send e-mail to interested parties
        logging.info('Send email to interested parties')
        _file_key_in_date_format = '{0}-{1}-{2}'.format(self.file_key[:4],
                                                        self.file_key[4:6],
                                                        self.file_key[6:8])
        _email_message = "Total attendance on {0}: {1}".format(_file_key_in_date_format, num_of_attendance)

        self.email_handler = EmailNotifier()
        self.email_handler.send_email(from_address='cacc.server@gmail.com',
                                      to_address_list=["hinchowliu@gmail.com", "cacc@cacc.org.nz",
                                                       "cacc.server@gmail.com"],
                                      subject='Attendance file uploaded to server {}'.format(_file_key_in_date_format),
                                      message=_email_message,
                                      file_attachment=full_path_out_filename)

        # Clean up after script loaded.
        self.remove_machine_generate_files()

    def create_folder_if_not_exist(self) -> None:
        """
        Ensure that the folder exists.
        """
        if not self.full_path_upload_folder.exists():
            os.mkdir(self.full_path_upload_folder)

    def remove_machine_generate_files(self):
        logging.info('Clean up machine generate files')
        files_to_remove = [os.path.join(self.full_path_upload_folder, f) for f in
                           os.listdir(self.full_path_upload_folder)]
        for f in files_to_remove:
            os.remove(f)

        if not self.full_path_upload_folder.exists():
            os.rmdir(self.full_path_upload_folder)


if __name__ == "__main__":
    AttendanceFileUploader()
