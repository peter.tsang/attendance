#!/usr/bin/env python3
"""
This file is used to create a new file (.txt) on every weekend or
"""
import click
import logging
import os
from pathlib import Path
from attendance_file import AttendanceFile

LOGGER_FOLDER = Path("attendance").absolute()
DEFAULT_FOLDER = LOGGER_FOLDER / "record"


@click.command()
@click.option('--override_create_key', type=click.STRING, help='Override create key')
@click.option('--verbose', '-v', is_flag=True, help='Enable verbose mode')
class AttendanceFileCreator:
    def __init__(self, override_create_key: None, verbose: False):
        self.override_create_key = override_create_key
        self.verbose = verbose
        self.attendance_file = AttendanceFile(self.override_create_key)

        # Get create key
        filename = self.attendance_file.get_next_weekend_date_filename()

        formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
        trace_logger = logging.getLogger()
        trace_logger.setLevel(logging.DEBUG)

        log_handler = logging.StreamHandler()
        log_handler.setLevel(logging.DEBUG)
        log_handler.setFormatter(formatter)
        trace_logger.addHandler(log_handler)

        # Get create key
        full_path_filename = DEFAULT_FOLDER / filename
        logging.debug('filename: {}'.format(full_path_filename))

        # Check if the folder is already existed. Create one if not existed.
        _create_folder_if_not_exist()
        # Check if file exists, otherwise create one.
        try:
            with open(full_path_filename):
                logging.info('{} is already existed'.format(full_path_filename))
        except IOError:
            open(full_path_filename, 'a').close()
            logging.info('{} is created'.format(full_path_filename))


def _create_folder_if_not_exist() -> None:
    """
    Ensure that the folder exists.
    """
    if not DEFAULT_FOLDER.exists():
        os.mkdir(DEFAULT_FOLDER)


if __name__ == "__main__":
    AttendanceFileCreator()
