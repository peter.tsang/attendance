#!/usr/bin/env python3
"""
This file is used to get next Sunday date.
"""
from datetime import datetime, timedelta


class AttendanceFile:
    def __init__(self, file_key):
        self.file_key = file_key

    def get_next_weekend_date_filename(self):
        """
        Get next weekend date based on given date.

        Returns:
            Current date if Sunday, or last Sunday info otherwise
        """
        file_key = self.get_attendance_file_key()
        next_sun_date = self.get_next_sunday_date(file_key)
        new_filename = next_sun_date + ".txt"
        return new_filename

    @staticmethod
    def get_datetime(key: str) -> datetime:
        """
        Convert the number string into date time format.
        Args:
            key: String in "YYYYMMDD" format.

        Returns:
            Datetime format of the value.
        """
        year = int(key[:4])
        month = int(key[4:6])
        date = int(key[6:8])
        return datetime(year, month, date)

    def get_next_sunday_date(self, key):
        next_sun_date = self.get_datetime(key)
        iso_day_of_week = self.get_iso_day_of_week(key)
        if iso_day_of_week != 7:
            next_sun_date += timedelta(7 - iso_day_of_week)
        return next_sun_date.strftime('%Y%m%d')

    def get_iso_day_of_week(self, key: str) -> int:
        """
        With a given string type of key, get iso day of week

        Args:
            key: in the format of YYYYMMDD

        Returns:
            iso day of week
        """
        key_datetime = self.get_datetime(key)
        iso_date_of_week = key_datetime.isoweekday()
        return iso_date_of_week

    def get_attendance_file_key(self):
        """
        Get create_key based on the following weekend date in the form of YYYYMMDD that
         appears anywhere in the filename.

        Returns:
            Create key in the format of "YYYYMMDD".
        """
        current_date = datetime.today().strftime('%Y%m%d')
        check_file_key = True
        try:
            if self.file_key is not None:
                val = int(self.file_key)
        except ValueError:
            check_file_key = False
        file_key = current_date if self.file_key is None or check_file_key is False else self.file_key
        return file_key
